mkdir -p bin/ffmpeg_content
wget -qO- https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz  | tar --strip-components=1 -xJvf - -C bin/ffmpeg_content
mv bin/ffmpeg_content/ffmpeg bin/ffmpeg
rm -rf bin/ffmpeg_content
chmod u+x bin/ffmpeg
