import os from 'os'
import fs from 'fs'
import * as child_process from 'child_process'

const supportedPlatforms = ['darwin', 'linux']
const isSupportedPlatform = supportedPlatforms.includes(os.platform())

function isInstalled (version) {
  if (os.platform() === 'darwin') {
    return child_process.spawnSync('brew', ['list', 'ffmpeg@' + version]).status === 0
  }

  if (os.platform() === 'linux') {
    return false
  } // default to false because we're going to download it anyway
}

function install (version) {
  if (os.platform() === 'darwin') {
    return child_process.spawnSync('brew', ['install', 'ffmpeg@' + version])
  }

  if (os.platform() === 'linux') {
    return child_process.spawnSync('bash', ['setup/linux_install.sh'])
  }
}

function getFfmpegBinPath (version) {
  if (os.platform() === 'darwin') {
    return child_process.spawnSync('brew', ['--prefix', 'ffmpeg@' + version]).stdout
      .toString()
      .trim() + '/bin/ffmpeg'
  }

  if (os.platform() === 'linux') {
    return 'bin/ffmpeg'
  }
}

function isBinProperVersion (binPath, version) {
  if (os.platform() === 'darwin' || os.platform() === 'linux') {
    return child_process.spawnSync(binPath, ['-version']).stdout
      .toString()
      .includes('ffmpeg version ' + version)
  }

  return false
}

function copyIntoPlace (ffmpegBinPath) {
  if (!fs.existsSync('bin')) {
    console.log('creating bin directory')
    fs.mkdirSync('bin')
  }

  if (fs.existsSync('bin/ffmpeg')) {
    console.log('replacing existing bin/ffmpeg')
    fs.unlinkSync('bin/ffmpeg')
  }

  console.log("Copying ffmpeg into place from ", fs.realpathSync(getFfmpegBinPath(5)), " to" +
    " bin/ffmpeg");
  fs.copyFileSync(fs.realpathSync(getFfmpegBinPath(5)), 'bin/ffmpeg')
}

/**
 * @darwin - we use brew install because pre-built downloaded binaries tend to fail in situations
 * where a proper build does not.
 */
if (!isSupportedPlatform) {
  /**
   * This should be simple enough to implement across all OS'
   */
  console.error('Your platform (' + os.platform() + ')is not yet supported. Please consider' +
    ' making a pull request.')
  process.exit(1)
}

const ffmpegVersion = '5'

if (isInstalled(ffmpegVersion)) {
  console.log('FFmpeg version ' + ffmpegVersion + ' is already installed.')
} else {
  console.log('Installing ffmpeg')
  const ffmpegInstallation = install(ffmpegVersion)

  if (ffmpegInstallation.status !== 0) {
    console.log(ffmpegInstallation.stderr.toString())
    process.exit(ffmpegInstallation.status)
  }
}

const ffmpegBinPath = getFfmpegBinPath(ffmpegVersion)
if (!isBinProperVersion(ffmpegBinPath, '5')) {
  console.error('Could not detect installed path for ffmpeg')
  process.exit(1)
}

/**
 * Linux version is downloaded directly to our desired path. no need to link
 */
if (os.platform() === 'darwin') {
  copyIntoPlace(ffmpegBinPath)
}
