import projectBinPath from "project-bin-path"
import * as path from 'path'

/**
 @returns string|null
 @param workingDirectory
 */
export function ffmpegPath(workingDirectory = "") {
  let cwd = workingDirectory
  if(!workingDirectory){
    cwd = process.cwd()
  }

  return projectBinPath(cwd).then(binPath => binPath ? path.join(binPath, 'ffmpeg') : null)
}
/**
 * @returns string|null
 * @param workingDirectory
 */
export function ffmpegPathSync(workingDirectory = "") {
  let cwd = workingDirectory
  if(!workingDirectory){
    cwd = process.cwd()
  }

  const binPath = projectBinPath.sync(cwd);
  return binPath ? path.join(binPath, 'ffmpeg') : null;
}
