# @garlikio/ffmpeg

`npm install @garlikio/ffmpeg`

For a specific version off FFmpeg: `npm install @garlikio/ffmpeg@1.0.0-beta.ffmpeg5.0`

> **MacOS Yarn Note:** Due to a known bug in Yarn that prevents the creation of the `node_modules/.bin/ffmpeg` link, you will need to run this after installing:
> 
> `yarn --check-files --ignore-scripts`
> 
> Consider adding it to your `postinstall` script.

## Features

Specify the version of the version of this package you and the FFmpeg version.

```bash
yarn add @garlikio/ffmpeg@{version}-beta.ffmpeg{ffmpeg_veersion}
```

> **Note:** Currently only FFmpeg version 5 has been added. Please submit your merge requests for new versions to the `install.js` file.
> 
> Also, only MacOS and Linux support has been added.

## Usage

Trigger your own process manually.

```js
import { ffmpegPathSync } from '@garlikio/ffmpeg'
// Async: import { ffmpegPath } from '@garlikio/ffmpeg'
import {spawn} from "child_process";

const ffmpegProcess = spawn(ffmpegPathSync(), ['-version'])
```

Or, use this FFmpeg wrapper. In this example we're streaming a local file to RTMP
```js
import ffmpeg, { ffmpegPathSync } from '@garlikio/ffmpeg'

const ffmpegArguments = [
  '-re', '-i', '-',
  '-c:v', 'libx264', '-preset', 'fast', '-strict', '-2',
  '-c:a', 'aac', '-b:a', '128k',
  '-f', 'flv',
  '-flvflags',  'no_duration_filesize',
    "rtmps://server.test"
]

const ffmpegProcess = ffmpeg(ffmpegArgs);

ffmpegProcess.onOutput((data) => process.stdout.write(data))
ffmpegProcess.onMessage((message) => console.log('message: ', message))
ffmpegProcess.onDisconnect(()=> console.log('Lost connection to ffmpeg'))
ffmpegProcess.onError((error)=> console.log('There was stderror:', error))
ffmpegProcess.onExit(code=> process.exit(code))

/**
 * Stream a file to rtmp
 */
const fileStream = fs.createReadStream("path/to/my.webm")
fileStream.on('error', () => {
  console.error('error')
  process.exit()
})

fileStream.on('open', (data)=> {
  ffmpegProcess.streamOut().on('error', function( err ) {
    console.log('stream out error', err)
    if (err.code === "EPIPE") {
      process.exit(0);
    }
  });

  const streamToRtmp = fileStream.pipe(ffmpegProcess.streamIn())
  streamToRtmp.on('error', (error)=> {
    console.log('stream error', error)
  })
});
```
