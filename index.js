'use strict'

import { spawn } from 'child_process'
import { ffmpegPathSync } from './src/ffmpegPath.js'

export * from './src/ffmpegPath.js'

/**
 * Spawn ffpmeg with custom arguments
 * @param ffmpegArgs
 * @param options
 * @returns {{onDisconnect: (function(*): ChildProcessWithoutNullStreams), streamOut: (function(): Readable), process: ChildProcessWithoutNullStreams, onStartStopManagementError: (function(*): ChildProcessWithoutNullStreams), onError: onError, onExit: (function(*): ChildProcessWithoutNullStreams), onOutput: (function(*): Readable), onSpawn: (function(*): ChildProcessWithoutNullStreams), onClose: (function(*): ChildProcessWithoutNullStreams), onMessage: (function(*): ChildProcessWithoutNullStreams), streamIn: (function(): Writable), write: (function(*): boolean), on: {(event: string, listener: (...args: any[]) => void): ChildProcessWithoutNullStreams, (event: "close", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "disconnect", listener: () => void): ChildProcessWithoutNullStreams, (event: "error", listener: (err: Error) => void): ChildProcessWithoutNullStreams, (event: "exit", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "message", listener: (message: Serializable, sendHandle: SendHandle) => void): ChildProcessWithoutNullStreams, (event: "spawn", listener: () => void): ChildProcessWithoutNullStreams, (event: "close", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "disconnect", listener: () => void): ChildProcessWithoutNullStreams, (event: "error", listener: (err: Error) => void): ChildProcessWithoutNullStreams, (event: "exit", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "message", listener: (message: Serializable, sendHandle: SendHandle) => void): ChildProcessWithoutNullStreams, (event: "spawn", listener: () => void): ChildProcessWithoutNullStreams, (event: "close", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "disconnect", listener: () => void): ChildProcessWithoutNullStreams, (event: "error", listener: (err: Error) => void): ChildProcessWithoutNullStreams, (event: "exit", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "message", listener: (message: Serializable, sendHandle: SendHandle) => void): ChildProcessWithoutNullStreams, (event: "spawn", listener: () => void): ChildProcessWithoutNullStreams, (event: "close", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "disconnect", listener: () => void): ChildProcessWithoutNullStreams, (event: "error", listener: (err: Error) => void): ChildProcessWithoutNullStreams, (event: "exit", listener: (code: (number | null), signal: (NodeJS.Signals | null)) => void): ChildProcessWithoutNullStreams, (event: "message", listener: (message: Serializable, sendHandle: SendHandle) => void): ChildProcessWithoutNullStreams}}}
 */
export default function ffmpeg(ffmpegArgs = [], options = {ffmpegPathOverride: ""}) {
  const {ffmpegPathOverride} = options
  let ffmpegExe = ffmpegPathSync()
  if(ffmpegPathOverride) {
    ffmpegExe = ffmpegPathOverride
  }

  const ffmpegProcess = spawn(ffmpegExe, ffmpegArgs)
  // child.stdin.pipe(process.stdin)
  // child.stdout.pipe(process.stdout)
  // child.stderr.pipe(process.stderr)

  ffmpegProcess.on('error', function (error) {
    console.log(ffmpegProcess)
  })

  return {
    on: ffmpegProcess.on,
    onExit: (onExit) => ffmpegProcess.on('exit', onExit),
    onDisconnect: (onDisconnect) => ffmpegProcess.on('disconnect', onDisconnect),
    onStartStopManagementError: (onError) => ffmpegProcess.on('error', onError),
    onError: (onError) => {
      let outputError = "";
      ffmpegProcess.stderr.on('data', (error)=> outputError += error.toString())
      ffmpegProcess.stderr.on('close', ()=>onError(outputError))
    },
    onMessage: (onMessage) => ffmpegProcess.on('message', onMessage),
    onSpawn: (onSpawn) => ffmpegProcess.on('spawn', onSpawn),
    onClose: (onClose) => ffmpegProcess.on('spawn', onClose),
    onOutput: (output) => ffmpegProcess.stdout.on('data', output),
    streamIn: () => ffmpegProcess.stdin,
    streamOut: () => ffmpegProcess.stdout,
    write: (data) => ffmpegProcess.stdin.write(data),
    process: ffmpegProcess,
  }
}
